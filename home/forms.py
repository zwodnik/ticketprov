from django import forms

# formularz rejstracji z polem oznaczającym ilość zamawianych biletów
class ReservationForm(forms.Form):
    count = forms.IntegerField(label="Pobierz już teraz", min_value=1, widget=forms.TextInput(attrs={ 'class':'form-control', 'placeholder':'Ilość' }))