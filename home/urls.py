from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^details/(?P<id>[0-9]+)/$', views.details, name='details'),
    url(r'^order-done/$', views.order_done, name='order-done'),
    url(r'^like/(?P<id>[0-9]+)/$', views.like, name='like'),
    url(r'^profil/$', views.profil, name='profil'),
    url(r'^tickets/$', views.tickets, name='tickets'),
    url(r'^ticket/(?P<id>[0-9]+)/$', views.ticket, name='ticket'),
    url(r'^remove/(?P<id>[0-9]+)/$', views.remove, name='remove'),
    url(r'', views.index, name='index'),
]
