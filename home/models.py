
from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Address(models.Model):
    country = models.CharField(max_length=32)
    city = models.CharField(max_length=32)
    street = models.CharField(max_length=64)
    home_number = models.IntegerField()
    home_sign = models.CharField(max_length=4, blank=True, null=True)
    flat_number = models.IntegerField(blank=True, null=True)
    created_by = models.ForeignKey(User, blank=True, null=True)

    def __str__(self):
        if self.flat_number is None:
            return "{0}, {1}, {2} {3}{4}".format(self.country, self.city, self.street, self.home_number, self.home_sign)
        return "{0}, {1}, {2} {3}{4}/{5}".format(self.country, self.city, self.street, self.home_number, self.home_sign, self.flat_number)


class Event(models.Model):

    title = models.CharField(max_length=32)
    description = models.TextField()
    cost = models.DecimalField(decimal_places=2, max_digits=9, blank=True, null=True)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    capacity = models.IntegerField()
    created_by = models.ForeignKey(User, default=None)
    address = models.ForeignKey(Address, default=None, blank=True, null=True)
    image = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.title

class Ticket(models.Model):
    name = models.CharField(max_length=32)
    code = models.CharField(max_length=128)
    order_datetime = models.DateTimeField()
    buyer = models.ForeignKey(User, default=None)
    active = models.BooleanField(default=True)
    event = models.ForeignKey(Event)

    def __str__(self):
        return self.code

# ulubione firmu uzytkownika
class LikedCompany(models.Model):
    user = models.ForeignKey(User, default=None, related_name='user')
    company = models.ForeignKey(User, default=None, related_name='company')


class Notifications(models.Model):
    owner = models.ForeignKey(User, default=None)
    event = models.ForeignKey(Event, default=None, related_name='event')
    readed = models.BooleanField(default=False)

