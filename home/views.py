# Create your views here.
import hashlib
import json

import qrcode
import base64
from io import BytesIO

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from .models import Event, Ticket, LikedCompany, Notifications
from datetime import datetime
from . import forms


def index(request):
        now = datetime.now()
        events_list = Event.objects.filter(
            start_datetime__gte=now.strftime('%Y-%m-%d %H:%I'))\
            .order_by('start_datetime')
        # parametr page został zapisany w pliku urls.py - domyślie wynosi 1
        page = request.GET.get('page', 1)

        # wyciąga do 9 wpisą na jedną stronę
        paginator = Paginator(events_list, 9)
        try:
                event = paginator.page(page)
        except PageNotAnInteger:
                event = paginator.page(1)
        except EmptyPage:
                event = paginator.page(paginator.num_pages)

        # notyfikacje do dokończenia
        try:
                notifications = Notifications.objects.filter(owner=request.user, readed=False)
        except:
                notifications = None

        return render(
            request, 'home/index.html',
            {'events': event, 'notifications': notifications})


def details(request, id):
        event = get_object_or_404(Event, id=id)
        event.deltatime = event.end_datetime - event.start_datetime

        notifications = Notifications.objects.filter(owner=request.user)
        for notification in notifications:
            notification.readed = True
            notification.save()

        busy = Ticket.objects.filter(event_id=event.id, active=True).count()
        event.avalible = event.capacity - busy
        # stworzenie obiektu formularza
        form = forms.ReservationForm()

        if request.user.is_authenticated:
                try:
                        # sprawdzenie czy uzytkownik lubi firme
                        like_company = bool(
                            LikedCompany.objects.get(
                                user=request.user,
                                company=event.created_by))
                except ObjectDoesNotExist:
                        like_company = False

                if request.method == 'POST':
                        # umieszczenie wartości w formularzu po przeładowaniu
                        # strony
                        form = forms.ReservationForm(request.POST)
                        # rozpoczęcie walidacji
                        if int(request.POST['count']) > event.avalible:
                                form.add_error(
                                    'count', "Nie możesz zamówić więcej niż jest dostępne")
                        if form.is_valid():
                                # wyciagniecie zwalidowanych danych
                                count = form.cleaned_data['count']
                                # zamówienie tylu biletow ile zamowil uzytkownik
                                for i in range(0, count):
                                        order_datetime = datetime.now()
                                        # stworzenie kodu biletu
                                        md5 = hashlib.md5(str("{0}{1}{2}".format(
                                                request.user.username, event.id, order_datetime)).encode('utf-8'))
                                        # wyciagniecie kodu jako liczy w hex
                                        code = md5.hexdigest()
                                        # stworzenie obiektu biletu
                                        Ticket.objects.create(
                                            name=event.title, code=code,
                                            order_datetime=order_datetime,
                                            buyer=request.user, active=True,
                                            event=event)
                                # przekierowanie do url ktorego name=order-done.
                                # opis jest w urls.py
                                return HttpResponseRedirect(
                                        reverse('order-done'))
                return render(
                    request, 'home/details.html',
                    {'event': event, 'form': form,
                     'like_company': like_company})

        return render(request, 'home/details.html',
                      {'event': event, 'form': form})


def order_done(request):
        return render(request, 'home/order-done.html')


def like(request, id):
        # metoda wywolywana nprzez AJAX(java script). zwraca objekt json
        if not request.user.is_authenticated:
                return HttpResponse(
                    json.dumps({
                        "code": "404",
                        "message": "Użytkownik musi być zalogowany"
                    }),
                    content_type="application/json"
                )
        if request.method == 'GET':
                try:
                        liked_company = LikedCompany.objects.get(
                                user=request.user, company=User.objects.get(id=id))
                        liked_company.delete()
                        return HttpResponse(
                            json.dumps({
                                "code": "200",
                                "message": "Usunięto firmę z polubionych",
                                "like": False
                            }),
                            content_type="application/json"
                        )
                except ObjectDoesNotExist:
                        LikedCompany.objects.create(
                            user=request.user,
                            company=User.objects.get(
                                id=id))
                        return HttpResponse(
                            json.dumps({
                                "code": "200",
                                "message": "Polubiono firmę",
                                "like": True
                            }),
                            content_type="application/json"
                        )
        else:
                return HttpResponse(
                    json.dumps({
                        "code": "404",
                        "message": "Niewłaściwa metoda żądania"
                    }),
                    content_type="application/json"
                )

# dodanie dekoratora wymagajacego zalogowanie uzytkownika. Jeśli nie jest
# zstanie przekierowany na podany ponizej adres. Gdy zostanie zalogowany
# zostanie przekierowany tam gdzie chcial sie dostac


@login_required(login_url='/account/login/')
def profil(request):
        liked_company = LikedCompany.objects.filter(user=request.user)

        return render(request, 'home/profil.html',
                      {'liked_company': liked_company})


@login_required(login_url='/account/login/')
def tickets(request):
        # wyciagniecie biletow ktore sa aktywne i wydarzenie jeszcze sie nie
        # rozpoczelo
        now = datetime.now()
        tickets = Ticket.objects.filter(
            buyer=request.user,
            event__start_datetime__gte=now.strftime('%Y-%m-%d %H:%I'),
            active=True
        )
        bulk = []
        for ticket in tickets:
                bulk.append(ticket.event_id)
        # wyciagniecie wydarzen na jakie wybiera sie uzytkownik
        events = Event.objects.filter(id__in=bulk)

        return render(request, 'home/tickets.html', {'events': events})


@login_required(login_url='/account/login/')
def ticket(request, id):
        # ticket = get_object_or_404(Ticket, buyer=request.user, id=id)
        event = get_object_or_404(Event, id=id)
        tickets = Ticket.objects.filter(
            event_id=id, buyer=request.user, active=True)

        for ticket in tickets:

                # stworzenie obiektu do generowania kodow qr
                qr = qrcode.QRCode(
                    version=1,
                    error_correction=qrcode.constants.ERROR_CORRECT_L,
                    box_size=12,
                    border=0,
                )

                # dodanie danych na podstawie ktorych kod zostanie wygenerowany
                qr.add_data(ticket.code)
                # stworzenie kodu qr
                qr.make(fit=True)

                # zapisanie wygenerowanego kodu do obrazka zapisanego w base64
                img = qr.make_image()
                buffer = BytesIO()
                img.save(buffer)
                ticket.encoded_img = base64.b64encode(buffer.getvalue())

        return render(request, 'home/ticket.html',
                      {'event': event, 'tickets': tickets})


@login_required(login_url='/account/login/')
def remove(request, id):
        # usuniecie biletu. Ustawienie biletu jako nieaktywny
        ticket = get_object_or_404(Ticket, id=id, buyer=request.user)
        ticket.active = False
        ticket.save()

        return render(request, 'home/remove-done.html')
