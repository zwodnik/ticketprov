from django.contrib import admin
from home.models import *
from django.contrib.auth.models import User


class FilterEventAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        if change:
            users_id = Ticket.objects.filter(active=True).values('buyer').distinct()
            users = User.objects.filter(id__in=users_id)
            for user in users:
                Notifications(owner=user, event=obj).save()
        obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        if not request.user.is_superuser:
            qs = super(FilterEventAdmin, self).get_queryset(request)
            return qs.filter(created_by=request.user)
        return super(FilterEventAdmin, self).get_queryset(request)

    def render_change_form(self, request, context, *args, **kwargs):
        if not request.user.is_superuser:
            context['adminform'].form.fields['address'].queryset = Address.objects.filter(created_by=request.user)
        return super(FilterEventAdmin, self).render_change_form(request, context, args, kwargs)


class FilterAddressAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        if not request.user.is_superuser:
            qs = super(FilterAddressAdmin, self).get_queryset(request)
            return qs.filter(created_by=request.user)
        return super(FilterAddressAdmin, self).get_queryset(request)


class EventAdmin(FilterEventAdmin):
    list_display = ('title', 'start_datetime', 'end_datetime', 'cost', 'capacity')
    exclude = ['created_by']


class AddressAdmin(FilterAddressAdmin):
    list_display = ('country', 'city', 'street', 'home_number', 'home_sign', 'flat_number')
    exclude = ['created_by']


# Register your models here.
admin.site.register(Ticket)
admin.site.register(Event, EventAdmin)
admin.site.register(Address, AddressAdmin)
