from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import re

class RegisterForm(forms.Form):
    username = forms.CharField(label="Login:", max_length=30, widget=forms.TextInput(attrs={ 'class':'form-control' }))
    email = forms.EmailField(label="Email:", widget=forms.EmailInput(attrs={ 'class':'form-control' }))
    password1 = forms.CharField(label="Hasło:", widget=forms.PasswordInput(attrs={ 'class':'form-control' }))
    password2 = forms.CharField(label="Powtórz hasło:", widget=forms.PasswordInput(attrs={ 'class':'form-control' }))

    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']
        if password1 == password2:
            return password1
        else:
            raise forms.ValidationError("Hasła nie są takie same")
        if len(password1) < 8:
            raise forms.ValidationError("Hasła musi mieć przynajmniej 8 znaków")

    def clean_username(self):
        username = self.cleaned_data['username']
        if not re.search(r'^[a-zA-Z0-9]+$', username):
            raise forms.ValidationError("Login może skaładać się tylko z liter i cyfr")

        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username

        raise forms.ValidationError("Podana nazwa użytkownika jest zajęta")

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            User.objects.get(email=email)
        except ObjectDoesNotExist:
            return email
        except:
            pass

        raise forms.ValidationError("Podany adres e-mail jest już używany")

class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Login:", max_length=30, widget=forms.TextInput(attrs={ 'class':'form-control' }))
    password = forms.CharField(label="Hasło:", widget=forms.PasswordInput(attrs={ 'class':'form-control' }))

class ChangePasswordForm(forms.Form):
    password1 = forms.CharField(label="Nowe hasło:", widget=forms.PasswordInput(attrs={ 'class':'form-control' }))
    password2 = forms.CharField(label="Powtórz hasło:", widget=forms.PasswordInput(attrs={ 'class':'form-control' }))

    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 == password2:
            return password1
        else:
            raise forms.ValidationError("Hasła nie są takie same")
        if len(password1) < 8:
            raise forms.ValidationError("Hasła musi mieć przynajmniej 8 znaków")
