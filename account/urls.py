from django.conf.urls import url
from django.contrib.auth import views as authviews
from . import views
from . import forms

urlpatterns = [
    url(r'^$', authviews.login, {'template_name': 'account/login.html', 'authentication_form': forms.LoginForm}, name='login'),
    url(r'^login/$', authviews.login, {'template_name': 'account/login.html', 'authentication_form': forms.LoginForm}, name='login-page'),
    url(r'^logout/$', authviews.logout, {'next_page': 'login-page'}, name='logout-page'),
    url(r'^register/$', views.register, name='register-page'),
    url(r'^register-done$', views.register_done, name='register-done-page'),
    url(r'^change-password$', views.change_password, name='change-password-page'),
    url(r'^change-password-done$', views.change_password_done, name='change-password-done-page'),
]