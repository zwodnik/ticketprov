from django.contrib.auth import login
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import Http404
from . import forms
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required


def register(request):
    if request.method == 'POST':
        form = forms.RegisterForm(request.POST)
        if form.is_valid():
            try:
                group = Group.objects.get(name='User')
            except ObjectDoesNotExist:
                raise Http404("Nie mozna dodac uzytkownika.\
Skontaktuj sie z Administratorem")

            user = User.objects.create_user(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password1'],
                email=form.cleaned_data['email'],
            )
            user.save()
            group.user_set.add(user)

            return HttpResponseRedirect(reverse('register-done-page'))
    else:
        form = forms.RegisterForm()
    return render(request, 'account/register.html', {'form': form})


def register_done(request):
    return render(request, 'account/register-done.html')


@login_required(login_url='/account/login/')
def change_password(request):
    if request.method == 'POST':
        form = forms.ChangePasswordForm(request.POST)
        if form.is_valid():
            request.user.set_password(form.cleaned_data['password2'])
            request.user.save()
            login(request, request.user)

            return HttpResponseRedirect(reverse('change-password-done-page'))
    else:
        form = forms.ChangePasswordForm()

    return render(request, 'account/change-password.html', {'form': form})


def change_password_done(request):
    return render(request, 'account/change-password-done.html')
