$(function() {

    $('.remove').on('click', function(event){
        var r = confirm("Czy na pewno chcesz usunąc bilet?");
        if (r == false) {
            event.preventDefault();
        }
    })

    $('#like').on('click', function(event){
        event.preventDefault();
        var that = this;
        var url = $(that).attr('href');

        $.ajax({
            url : url,
            type : "GET",
        }).done(function(data) {
            // console.log(data.code);
            // console.log(data.message);

            if(data.like)
                $(that).find("span").addClass("glyphicon-heart").removeClass("glyphicon-heart-empty");
            else
                $(that).find("span").addClass("glyphicon-heart-empty").removeClass("glyphicon-heart");

            $(that).blur();
        });

    });

});